# gappi: Get A PhilPapers Item #

gappi is a very simple Bash script, which uses curl to conduct a search on PhilPapers ([https://philpapers.org](https://philpapers.org)), and output the BibTeX record(s) for the search result(s).

## Installation ##

*Requirements*: Bash is the default shell for most forms of *nix, including Mac OS X, and most linux distributions; if you’re using one of those, you have it already. You also need [curl](https://curl.haxx.se/). I think curl comes pre-installed on OS X; it comes pre-installed on many linux distributions as well, and in any case is almost certainly available through your distribution’s package repository. Windows users can probably use Cygwin, and its curl package, though this has not been tested.

### Steps to install: ###

Acquire the script. You may do this in many ways such as using the clone or download links on the left. (You need only the `gappi` file.) You might also fetch it with curl, which would have the added advantage of ensuring that curl is properly installed on your system:

`curl https://bitbucket.org/frabjous/gappi/raw/master/gappi -o gappi`

Move the script to one of the folders listed in your $PATH environmental variable; the folder $HOME/bin is a good choice if available.

Make sure the script is executable: 

`chmod 755 gappi`

Execute the command `gappi --help` to ensure that it is ready to use.

## Examples of usage ##

gappi is first and foremost a command-line tool. It takes one or more arguments and outputs the BibTeX record for first/most-relevant result in PhilPaper’s database.

For example, the command below would result in the following output: (In the examples below, the initial $ just indicates the terminal prompt; it is not part of the command.)

    :::text
    $ gappi "Kaplan How to Russell a Frege-Church"
    @article{Kaplan1975-KAPHTR,
        title = {How to Russell a Frege-Church},
        journal = {Journal of Philosophy},
        pages = {716--729},
        volume = {72},
        year = {1975},
        author = {David Kaplan},
        number = {19}
    }

gappi outputs to stdout, but you can use normal i/o redirection techniques to create or append to a file. For instance, the following could be used to add the Kaplan entry to a BibTeX .bib file:

`$ gappi "Kaplan How to Russell a Frege-Church" >> myreferences.bib`

(Be sure to use >> rather than just > to avoid overwriting the file.)

You can use the more complicated syntax for PhilPapers searches listed at [https://philpapers.org/help/search.html](https://philpapers.org/help/search.htmlLink URL). For example:

    :::text
    $ gappi "@authors Eklund @title Inconsistent Languages"
    @article{Eklund2002-EKLIL,
        year = {2002},
        journal = {Philosophy and Phenomenological Research},
        title = {Inconsistent Languages},
        author = {Matti Eklund},
        volume = {64},
        pages = {251--275},
        number = {2}
    }

Note that it is “`@authors`” with an “`s`” not “`@author`”, even when there is only one. It is a good idea to use this syntax as much as possible, especially for books, as otherwise you are likely to a get an entry for a book review, rather than the entry for the book itself.

Thanks to improvements by David Sanson, you can retrieve more than one record for your search. To do this use `gappi -n #` prior to your search query, replacing `#` with the number of records you want to retrieve.

    :::text
    $ gappi -n 2 "@authors Bertrand Russell @title Truth"
    @incollection{Russell1992-RUSWJC,
        publisher = {Routledge},
        editor = {William James and Doris Olin},
        author = {Bertrand Russell},
        title = {William James's Conception of Truth},
        year = {1992},
        booktitle = {William James: Pragmatism, in Focus}
    }
    
    @book{Russell1995-RUSAII,
        publisher = {Routledge},
        author = {Bertrand Russell},
        title = {An Inquiry Into Meaning and Truth: The William James Lectures for 1940 Delivered at Harvard University},
        year = {1995}
    }

You also can do more than one search at a time. The parameter for each search should be demarcated with its own quotation marks. For a literal ", escape it with a backslash \".

    :::text
    $ gappi "@authors Stalnaker @title Presuppositions" "@authors Lewis @title \"scorekeeping in a language game\""
    @article{Stalnaker1973-STAP-5,
        author = {Robert Stalnaker},
        title = {Presuppositions},
        year = {1973},
        journal = {Journal of Philosophical Logic},
        number = {4},
        volume = {2},
        pages = {447--457}
    }
    
    @article{Lewis1979-LEWSIA,
        author = {David Lewis},
        year = {1979},
        number = {1},
        title = {Scorekeeping in a Language Game},
        volume = {8},
        journal = {Journal of Philosophical Logic},
        pages = {339--359}
    }


Proficient command-line users can do no doubt think of other things to do as well.

If there are no search results, gappi will just output a blank line.

## Integration with editors such as vim ##

When I was a vim user, I added the following to my ~/.vimrc:

`autocmd BufNewFile,BufRead *.bib nnoremap _ :r!gappi ""<Left>`

When editing .bib files, this maps the underscore key to invoke gappi. I need only type my search parameters, and press enter, and the resulting record is inserted into my document.

Similar things could no doubt be programmed for other “smart” text editors which allow one to invoke external commands.

## Limitations and frustrations ##

gappi is only as good as the PhilPapers search ranking algorithms and BibTeX export routine allow it to be. Unfortunately, these could be improved significantly. That PhilPapers never seems to include the address field for `@Book` entries is particularly annoying, as is its tendency to treat a book review as more relevant than the book itself. At least the latter can usually be circumvented with additional search parameters. However, it would be best for those of us with any technical know-how to stop complaining and volunteer to improve PhilPapers.

## Improving gappi ##

If you know how to use bitbucket and/or git, feel free to push changes here for approval.

For others, if have any suggestions for improvement, etc., or other feedback, [email Kevin](mailto:klement@umass.edu), or post to the LaTeXoSoPHeRs Facebook group. Thanks to David Sanson for suggesting improvements to a previous version.

## License ##

gappi is made available under the [GNU/General Programming License (GPL) v3](http://www.gnu.org/licenses/gpl.html). You may edit it, redistribute it, etc., in any way you see fit.